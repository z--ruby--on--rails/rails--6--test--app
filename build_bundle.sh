#!/bin/sh

touch .building

source /home/${LOCAL_USER}/.bashrc
bundle install --jobs 4 --retry 4

rm .building
